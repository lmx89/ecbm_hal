# EXTI库

## 简介

EXTI的全称是External interrupt/event controller——外部中断/事件控制器。8051单片机一共有两个外部中断脚，分别为外部中断0（P3.2脚）和外部中断1（P3.3脚）。本库涵盖了外部中断外设的以下功能：

- 配置

- 操作

- 初始化

- 反初始化

## 外设属性

外部中断的属性如下表所示：

|    属性     |                     说明                     |
| :---------: | :------------------------------------------: |
|     id      |  外设编号，0代表外部中断0，1代表外部中断1。  |
|  it_enable  |       中断使能，0代表关闭，1代表开启。       |
|   it_mode   | 中断模式，0代表低电平中断，1代表下降沿中断。 |
| it_priority |  中断优先级，0代表低优先级，1代表高优先级。  |

可以通过exti_typedef来定义一个外部中断信息结构体用于存放外部中断的属性信息，例如

```c
exti_typedef it_p32; //定义一个信息结构体，名字叫it_p32。
it_p32.id=0;         //设置本结构体为外部中断0的信息结构体。
it_p32.it_enable=1;  //打开外部中断0的中断使能。
it_p32.it_mode=1;    //设置外部中断0的中断模式为下降沿中断。
it_p32.it_priority=1;//设置外部中断0的中断优先级为高。
```

## 函数返回信息

库函数在执行完毕都会返回一个信息，用于反馈执行的结果。目前的信息有：

### EXTI_OK

函数执行无误。

### EXTI_ID_ERR

外部中断的编号错误。

比如8051单片机没有外部中断2，但是编号却设置为2，那么执行库函数就会返回EXTI_ID_ERR。

如果输入的编号确实是合法的，那么请检查该编号的代码是不是被宏定义优化了。比如8051单片机确实有外部中断1，但是编号为1时返回EXTI_ID_ERR，可能是因为宏定义ECBM_EXTI1_LIB_EN为0了。

## 函数API

### exti_init

函数原型：exti_status exti_init(exti_typedef * dev)；

#### 描述

外部中断初始化函数，用于将信息结构体的信息写到外部中断硬件中。

#### 输入

- dev：外部中断信息结构体。

#### 输出

无

#### 返回值

- EXTI_OK：正常
- EXTI_ID_ERR：编号错误

#### 调用例程

```c
exti_typedef it_p32; //定义一个信息结构体，名字叫it_p32。
it_p32.id=0;         //设置本结构体为外部中断0的信息结构体。
it_p32.it_enable=1;  //打开外部中断0的中断使能。
it_p32.it_mode=1;    //设置外部中断0的中断模式为下降沿中断。
it_p32.it_priority=1;//设置外部中断0的中断优先级为高。
exti_init(&it_p32);  //使用以上信息初始化外部中断0。
```

#### 注意事项

1. 在执行初始化函数之前，要先对外设的属性定义好预期的值。可以手动填写：
   
   ```c
   it_p32.id=0;         //ID为0，对应着外部中断0，也就是P3.2脚。
   it_p32.it_enable=1;  //使能外部中断0。
   it_p32.it_mode=1;    //下降沿中断。
   it_p32.it_priority=0;//中断为低优先级。
   ```
   
   也可以用函数填充：
   
   ```c
   exti_get_configuration_wizard(&it_p32,0);//将图形化界面的外部中断0的配置信息填充到it_p32中。
   ```
   
2. 由于外部中断信息结构体可以多次定义。但由于硬件只有一个，所以当两个信息结构体初始化到同一个外部中断时，比如：

   ```c
   exti_typedef it_p32,key_power;//定义两个外部中断信息结构体，一个为it_p32，另一个为key_power。
   it_p32.id=0;                  //ID为0，对应着外部中断0，也就是P3.2脚。
   it_p32.it_enable=1;           //使能外部中断0。
   it_p32.it_mode=0;             //低电平中断。
   exti_init(&it_p32);           //初始化
   key_power.id=0;               //ID为0，对应着外部中断0，也就是P3.2脚。
   key_power.it_enable=0;        //不使能外部中断0，仅做配置用。
   key_power.it_mode=1;          //下降沿中断。
   exti_init(&key_power);        //初始化
   ```

   那么后面初始化的属性就会把前面初始化的属性替换掉。也就是说执行上面9句代码后，外部中断0的属性是信息结构体key_power的值。多个信息结构体映射到同一个外设其实也是外设复用的一个办法，只要合理的分配初始化的时机就行。比如制作一个XX模块，模块的控制脚可以用单片机控制也可以用按键控制，就可以这样：

   ```c
   if(mode_check()==MOD_MODE_IO){//执行检测函数，判断模块当前是不是处于IO控制模式。
       exti_init(&it_p32);       //是IO控制模式，就按it_p32的属性来初始化外部中断0。
   }else{                        //不是IO控制就是按键控制，
       exti_init(&key_power);    //就按key_power的属性来初始化外部中断0。
   }
   ```

3. 如果属性it_enable为1，那么在初始化的时候就会打开外部中断。

### exti_deinit

函数原型：exti_status exti_deinit(u8 id)；

#### 描述

外部中断还原函数，用于还原外部中断的设置到上电默认值。

#### 输入

- id：外部中断编号。


#### 输出

无

#### 返回值

- EXTI_OK：正常
- EXTI_ID_ERR：编号错误

#### 调用例程

```c
exti_deinit(0);//将外部中断0还原成上电默认的状态。
```

#### 注意事项

1. 本函数是直接操作外设寄存器的，不会修改信息结构体。
2. 由于不同单片机的外部中断设置可能有一些差别，所以执行本函数之后，和真正的上电状态会有一点点差别。

### exti_start

函数原型：exti_status exti_start(exti_typedef * dev)；

#### 描述

外部中断开启函数，用于打开外部中断。

#### 输入

- dev：外部中断信息结构体。


#### 输出

- dev：外部中断信息结构体。


#### 返回值

- EXTI_OK：正常
- EXTI_ID_ERR：编号错误

#### 调用例程

```c
exti_start(&it_p32); //打开外部中断0。
```

#### 注意事项

1. 本函数执行后，会修改信息结构体的it_enable值为1。

### exti_stop

函数原型：exti_status exti_stop(exti_typedef * dev)；

#### 描述

外部中断关闭函数，用于关闭外部中断。

#### 输入

- dev：外部中断信息结构体。


#### 输出

- dev：外部中断信息结构体。


#### 返回值

- EXTI_OK：正常
- EXTI_ID_ERR：编号错误

#### 调用例程

```c
exti_stop(&it_p32); //关闭外部中断0。
```

#### 注意事项

1. 本函数执行后，会修改信息结构体的it_enable值为0。

### exti_get_configuration_wizard

函数原型：exti_status exti_get_configuration_wizard(exti_typedef * dev,u8 id)；

#### 描述

从图形化界面中读取外部中断信息函数。

#### 输入

- id：需要读取的外部中断编号。


#### 输出

- dev：外部中断信息结构体。


#### 返回值

- EXTI_OK：正常
- EXTI_ID_ERR：编号错误

#### 调用例程

```c
exti_typedef exti0; //定义一个信息结构体。
exti_get_configuration_wizard(&exti0,0);//从图形化界面里读取外部中断0的设置信息。
exti_init(&exti0);  //使用以上信息初始化外部中断0。
```

#### 注意事项

1. 图形化界面是KEIL的一个功能，请打开KEIL来体验该功能。
2. 用KEIL打开exti.h，会在编辑框左下角看到两个标签，“Text Editor”和“Configuration Wizard”。点击“Configuration Wizard”标签就能看到图形化界面。而点击“Text Editor”标签就能回到文字编辑界面。

## 使用案例

### 案例功能

本案例实现了通过按键改变LED的亮灭状态，按一下按键，LED由灭变亮；再按一下按键，LED又由亮变灭，如此循环。

### 原理图

![原理图](https://gitee.com/ecbm/ecbm_hal/raw/master/docs/%E5%9B%BE%E7%89%87%E7%B4%A0%E6%9D%90/%E5%9B%BE1.png)

### 图形界面法

1. 打开KEIL for C51，用KEIL打开hal_config.h，进入图形化界面，勾选【外设库优化】里的【EXTI----外部中断库】。

2. 打开hal_exti.h，进入图形化界面，根据需要勾选【外部中断0函数库使能】或者【外部中断1函数库使能】。本例中用到P3.2脚所以勾选【外部中断0函数库使能】。

3. 在此界面根据需求修改【使能】、【中断模式】和【中断优先级】这几个设置。本例中通过按键产生中断来切换LED状态，所以【使能】要打开。按键按下会产生一个电压下降沿，所以【中断模式】选择**下降沿中断**。目前只有一个中断，所以【中断优先级】维持默认值。

4. 打开main.c，输入如下代码：

   ```c
   #include "hal_config.h" //hal库配置头文件。
   void main(void){        //主函数。
       exti_typedef exti0; //定义一个信息结构体。
       system_init();      //系统初始化。
       exti_get_configuration_wizard(&exti0,0);
       exti_init(&exti0);  //使用以上信息初始化外部中断0。
       while(1){           //主循环。
       }
   }
   void it0_fun() EXTI0_IT_NUM {//外部中断0中断服务函数。
       P00=!P00;                //取反P00的电平就是切换LED的状态。
   }
   ```

5. 编译出HEX文件，下载到单片机或者导入到仿真软件内即可看到效果。

### 文字界面法

1. 打开VSCode，用VSCode打开hal_config.h，将宏定义ECBM_CONFIG_EXTI_EN的值设为1。

2. 打开hal_exti.h，根据需要修改宏定义ECBM_EXTI0_LIB_EN或者ECBM_EXTI1_LIB_EN。本例中用到P3.2脚所以设置ECBM_EXTI0_LIB_EN为1。

3. 打开main.c，输入如下代码：

   ```c
   #include "hal_config.h" //hal库配置头文件。
   void main(void){        //主函数。
       exti_typedef exti0; //定义一个信息结构体。
       system_init();      //系统初始化。
       exti0.id=0;         //设置本结构体为外部中断0的信息结构体。
       exti0.it_enable=1;  //打开外部中断0的中断使能。
       exti0.it_mode=1;    //设置外部中断0的中断模式为下降沿中断。
       exti0.it_priority=0;//设置外部中断0的中断优先级为低。
       exti_init(&exti0);  //使用以上信息初始化外部中断0。
       while(1){           //主循环。
       }
   }
   void it0_fun() EXTI0_IT_NUM {//外部中断0中断服务函数。
       P00=!P00;                //取反P00的电平就是切换LED的状态。
   }
   ```

4. 编译出HEX文件，下载到单片机或者导入到仿真软件内即可看到效果。

### 效果演示

![演示图](https://gitee.com/ecbm/ecbm_hal/raw/master/docs/%E5%9B%BE%E7%89%87%E7%B4%A0%E6%9D%90/%E5%9B%BE2.gif)