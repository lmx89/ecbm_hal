# UART库

## 简介

UART的全称是**U**niversal **A**synchronous **R**eceiver/**T**ransmitter——通用异步收发传输器。也就是俗称的**串口**。8051单片机有一个串口，不同于定时器，串口是从1开始数起的。其中接收脚是P3.0脚，发送脚是P3.1脚。本库涵盖了外部中断外设的以下功能：

- 配置

- 操作

- 初始化

- 反初始化

## 外设属性

串口的属性如下表所示：

|    属性     |                             说明                             |
| :---------: | :----------------------------------------------------------: |
|     id      |                    外设编号，1代表串口1。                    |
|  it_enable  |               中断使能，0代表关闭，1代表开启。               |
| it_priority |          中断优先级，0代表低优先级，1代表高优先级。          |
| rxd_enable  |      接收使能，0代表串口不接收数据，1代表串口接收数据。      |
|  baud_mul   |       波特率加倍，0代表波特率不加倍，1代表波特率加倍。       |
| mux_enable  |       多机通信使能，0代表仅一对一通信，1代表多机通信。       |
|  run_mode   | 运行模式，0代表同步移位，1代表可变波特率8位通信，2代表固定波特率9位通信，3代表可变波特率9位通信。 |
|    check    | 校验方式，0代表不校验，1代表奇校验，2代表偶校验，3代表1校验，4代表0校验，5代表自定义校验。 |
| init_value  |        波特率初始，是用于写入定时器产生波特率的初值。        |

可以通过uart_typedef来定义一个串口信息结构体用于存放串口的属性信息，例如

```c
uart_typedef uart1;  //定义一个信息结构体，名字叫uart1。
uart1.id         =1; //设置本结构体为串口1的信息结构体。
uart1.it_enable  =1; //打开串口1的中断使能。
uart1.it_priority=0; //设置串口1的中断优先级为低。
uart1.rxd_enable =1; //打开串口1的接收使能。
uart1.baud_mul   =0; //设置串口1的波特率不加倍。
uart1.mux_enable =0; //关闭串口1的多机通信使能。
uart1.run_mode   =1; //设置串口1的工作模式为可变波特率8位通信。
uart1.check      =1; //设置串口1的校验方式为无校验。
uart1.check      =0xFDFD;//设置串口1的定时初值，这个初值对应9600bps@11.0592MHz。
```

## 函数返回信息

库函数在执行完毕都会返回一个信息，用于反馈执行的结果。目前的信息有：

### UART_OK

函数执行无误。

### UART_ID_ERR

串口的编号错误。

比如8051单片机的串口是从1开始数起的，编号设置为0的话，执行库函数就会返回UART_ID_ERR。

如果输入的编号确实是合法的，那么请检查该编号的代码是不是被宏定义优化了。比如8051单片机确实有串口1，但是编号为1时返回UART_ID_ERR，可能是因为宏定义ECBM_UART1_LIB_EN为0了。

### UART_BAUD_ERR

波特率错误。

限于8051的定时器只有8位的自动重载，所以并不是所有的波特率都能支持。比如在12MHz晶振下，只有1200、2400、4800这3种波特率可用。如果超过了支持的范围，就会返回该错误。

### UART_CHECK_ERR

校验错误。

目前的版本仅限用于在初始化的时候，检测设置的校验方式是否符合预设值。

## 函数API

### uart_init

函数原型：uart_status uart_init(uart_typedef * dev)；

#### 描述

串口初始化函数，用于将信息结构体的信息写到串口硬件中。

#### 输入

- dev：串口信息结构体。

#### 输出

无

#### 返回值

- UART_OK：正常
- UART_ID_ERR：编号错误

#### 调用例程

```c
uart_typedef uart1;  //定义一个信息结构体，名字叫uart1。
uart1.id         =1; //设置本结构体为串口1的信息结构体。
uart1.it_enable  =1; //打开串口1的中断使能。
uart1.it_priority=0; //设置串口1的中断优先级为低。
uart1.rxd_enable =1; //打开串口1的接收使能。
uart1.baud_mul   =0; //设置串口1的波特率不加倍。
uart1.mux_enable =0; //关闭串口1的多机通信使能。
uart1.run_mode   =1; //设置串口1的工作模式为可变波特率8位通信。
uart1.check      =1; //设置串口1的校验方式为无校验。
uart1.check      =0xFDFD;//设置串口1的定时初值，这个初值对应9600bps@11.0592MHz。
uart_init(&uart1);   //使用以上信息初始化串口1。
```

#### 注意事项

1. 在执行初始化函数之前，要先对外设的属性定义好预期的值。可以手动填写：
   
   ```c
   uart1.id         =1; //设置本结构体为串口1的信息结构体。
   uart1.it_enable  =1; //打开串口1的中断使能。
   uart1.it_priority=0; //设置串口1的中断优先级为低。
   uart1.rxd_enable =1; //打开串口1的接收使能。
   uart1.baud_mul   =0; //设置串口1的波特率不加倍。
   uart1.mux_enable =0; //关闭串口1的多机通信使能。
   uart1.run_mode   =1; //设置串口1的工作模式为可变波特率8位通信。
   uart1.check      =1; //设置串口1的校验方式为无校验。
   uart1.check      =0xFDFD;//设置串口1的定时初值，这个初值对应9600bps@11.0592MHz。
   ```
   
   也可以用函数填充：
   
   ```c
   uart_get_configuration_wizard(&uart1,1);//将图形化界面的串口1的配置信息填充到uart1中。
   ```
   
2. 由于串口信息结构体可以多次定义。但由于硬件只有一个，所以当两个信息结构体初始化到同一个串口时，比如：

   ```c
   uart_typedef uart1_4800,uart1_9600;  //定义一个信息结构体，名字叫uart1。
   uart1_4800.id         =1; //设置本结构体为串口1的信息结构体。
   uart1_4800.it_enable  =1; //打开串口1的中断使能。
   uart1_4800.it_priority=0; //设置串口1的中断优先级为低。
   uart1_4800.rxd_enable =1; //打开串口1的接收使能。
   uart1_4800.baud_mul   =0; //设置串口1的波特率不加倍。
   uart1_4800.mux_enable =0; //关闭串口1的多机通信使能。
   uart1_4800.run_mode   =1; //设置串口1的工作模式为可变波特率8位通信。
   uart1_4800.check      =1; //设置串口1的校验方式为无校验。
   uart1_4800.check      =0xFAFA;//设置串口1的定时初值，这个初值对应4800bps@11.0592MHz。
   uart_init(&uart1_4800);   //使用以上信息初始化串口1。
   uart1_9600.id         =1; //设置本结构体为串口1的信息结构体。
   uart1_9600.it_enable  =1; //打开串口1的中断使能。
   uart1_9600.it_priority=0; //设置串口1的中断优先级为低。
   uart1_9600.rxd_enable =1; //打开串口1的接收使能。
   uart1_9600.baud_mul   =0; //设置串口1的波特率不加倍。
   uart1_9600.mux_enable =0; //关闭串口1的多机通信使能。
   uart1_9600.run_mode   =1; //设置串口1的工作模式为可变波特率8位通信。
   uart1_9600.check      =1; //设置串口1的校验方式为无校验。
   uart1_9600.check      =0xFDFD;//设置串口1的定时初值，这个初值对应9600bps@11.0592MHz。
   uart_init(&uart1_9600);   //使用以上信息初始化串口1。
   ```

   那么后面初始化的属性就会把前面初始化的属性替换掉。也就是说执行上面21句代码后，串口1的属性是信息结构体uart1_9600的值。多个信息结构体映射到同一个外设其实也是外设复用的一个办法，只要合理的分配初始化的时机就行。比如制作一个XX模块，模块的波特率可以用外部焊盘来选择，焊上焊盘模块波特率为4800，不焊上就是9600，就可以这样：

   ```c
   if(mode_check()==0){       //执行检测函数（假设焊盘焊上为低电平）。
       uart_init(&uart1_4800);//焊盘焊上就工作在4800波特率下。
   }else{                     //没焊上，
       uart_init(&uart1_9600);//就工作在9600波特率下。
   }
   ```

3. 由于发送和接收中断都需要清零对应的标志位，所以强烈建议设置it_enable为1。

### uart_deinit

函数原型：uart_status uart_deinit(u8 id)；

#### 描述

串口还原函数，用于还原串口的设置到上电默认值。

#### 输入

- id：串口编号。


#### 输出

无

#### 返回值

- UART_OK：正常
- UART_ID_ERR：编号错误

#### 调用例程

```c
uart_deinit(1);//将串口还原成上电默认的状态。
```

#### 注意事项

1. 本函数是直接操作外设寄存器的，不会修改信息结构体。
2. 由于不同单片机的串口设置可能有一些差别，所以执行本函数之后，和真正的上电状态会有一点点差别。
3. 本函数执行后会关掉定时器1。

### uart_config

函数原型：uart_status uart_config(uart_typedef * dev,u32 baud,u8 check)；

#### 描述

串口配置函数，可以将串口配置成最常用的异步通信模式，同时还可以计算波特率。

#### 输入

- dev：串口信息结构体。
- baud：波特率。
- check：校验方式。

#### 输出

- dev：串口信息结构体。


#### 返回值

- UART_OK：正常
- UART_BAUD_ERR：波特率错误
- UART_CHECK_ERR：校验错误。

#### 调用例程

```c
uart_typedef uart1;  //定义一个信息结构体，名字叫uart1。
uart1.id         =1; //设置本结构体为串口1的信息结构体。
uart_config(&uart1,uart_baud_4800,uart_check_none);//配置成4800波特率无校验。
uart_init(&uart1);   //使用以上信息初始化串口1。
```

#### 注意事项

1. 本函数执行后，会修改信息结构体的it_enable值为1。
2. 本函数执行后，会修改信息结构体的it_priority值为0。
3. 本函数执行后，会修改信息结构体的rxd_enable值为1。
4. 本函数执行后，会修改信息结构体的mux_enable值为0。
5. 本函数仅做配置参数用，计算好的参数会保存到信息结构体中，所以还是需要执行uart_init函数将参数写入硬件中才会生效。
6. 本函数不会修改ID，因此需要提前设置好ID的值。

### uart_char

函数原型：uart_status uart_char(uart_typedef * dev,u8 dat)；

#### 描述

单个字符发送函数，同时还能根据设置自动加上校验位。

#### 输入

- dev：串口信息结构体。
- dat：要发送的数据。

#### 输出

无

#### 返回值

- UART_OK：正常
- UART_ID_ERR：编号错误
- UART_CHECK_ERR：校验错误。

#### 调用例程

```c
uart_char(&uart1,'Q'); //使用串口1发送字符Q。
```

#### 注意事项

1. 本函数不支持自定义校验，所以选择了自定义校验的话就会返回UART_CHECK_ERR。

### uart_char_8

函数原型：uart_status uart_char_8(uart_typedef * dev,u8 dat)；

#### 描述

单个字符发送函数，不含校验位。

#### 输入

- dev：串口信息结构体。
- dat：要发送的数据。

#### 输出

无

#### 返回值

- UART_OK：正常
- UART_ID_ERR：编号错误

#### 调用例程

```c
uart_char_8(&uart1,'Q'); //使用串口1发送字符Q。
```

#### 注意事项

1. 本函数是不加校验的版本，运行速度会比uart_char快一点点。

### uart_char_9

函数原型：uart_status uart_char_9(uart_typedef * dev,u8 dat,u8 bit9)；

#### 描述

单个字符发送函数，含自定义校验位。

#### 输入

- dev：串口信息结构体。
- dat：要发送的数据。
- bit9：第九位检验位的值。

#### 输出

无

#### 返回值

- UART_OK：正常
- UART_ID_ERR：编号错误

#### 调用例程

```c
uart_char_9(&uart1,'Q',1); //使用串口1发送字符Q，第九位为1。
uart_char_9(&uart1,'A',0); //使用串口1发送字符Q，第九位为0。
```

#### 注意事项

1. 本函数是自定义校验的版本，由于自定义的自由度很大，所以没有提供基于uart_char_9的上层库函数（比如uart_string函数），需要自己写代码实现。

### uart_string

函数原型：uart_status uart_string(uart_typedef * dev,u8 * str)；

#### 描述

字符串发送函数，同时还能根据设置自动加上校验位。

#### 输入

- dev：串口信息结构体。
- str：要发送的字符串。

#### 输出

无

#### 返回值

- UART_OK：正常

#### 调用例程

```c
uart_string(&uart1,"Hi,PC!\r\n"); //使用串口1发送字符串"Hi,PC!"并回车换行。
```

#### 注意事项

1. 本函数不支持自定义校验，选择了自定义校验的话就会以无校验来运行。

### uart_get_configuration_wizard

函数原型：uart_status uart_get_configuration_wizard(uart_typedef * dev,u8 id)；

#### 描述

从图形化界面中读取串口信息函数。

#### 输入

- id：需要读取的串口编号。


#### 输出

- dev：串口信息结构体。


#### 返回值

- UART_OK：正常
- UART_ID_ERR：编号错误

#### 调用例程

```c
uart_typedef uart1; //定义一个信息结构体。
uart_get_configuration_wizard(&uart1,1);//从图形化界面里读取串口1的设置信息。
uart_init(&uart1);  //使用以上信息初始化串口1。
```

#### 注意事项

1. 图形化界面是KEIL的一个功能，请打开KEIL来体验该功能。
2. 用KEIL打开uart.h，会在编辑框左下角看到两个标签，“Text Editor”和“Configuration Wizard”。点击“Configuration Wizard”标签就能看到图形化界面。而点击“Text Editor”标签就能回到文字编辑界面。
3. 如果在优化界面里关闭了uart_config函数的话，在执行本函数之后还需要手动填写定时初值和波特率加倍设置。

## 使用案例

### 案例功能

本案例实现了通过外接串口助手，接收并显示串口数据。单片机以波特率9600@11.0592MHz循环发送字符串“Hi,PC!”。

### 原理图



### 图形界面法

1. 打开KEIL for C51，用KEIL打开hal_config.h，进入图形化界面，勾选【外设库优化】里的【UART----串口库】。

2. 打开hal_uart.h，进入图形化界面，勾选【串口1函数库使能】。

3. 在此界面根据需求修改【使能】、【中断模式】和【中断优先级】这几个设置。本例中通过按键产生中断来切换LED状态，所以【使能】要打开。按键按下会产生一个电压下降沿，所以【中断模式】选择**下降沿中断**。目前只有一个中断，所以【中断优先级】维持默认值。

4. 打开main.c，输入如下代码：

   ```c
   #include "hal_config.h" //hal库配置头文件。
   void main(void){        //主函数。
       uart_typedef uart1; //定义一个信息结构体。
       system_init();      //系统初始化。
       uart_get_configuration_wizard(&uart1,1);
       uart_init(&uart1);  //使用以上信息初始化串口1。
       while(1){           //主循环。
           uart_string(&uart1,"Hi,PC!\r\n");
       }
   }
   ```
   
5. 编译出HEX文件，下载到单片机或者导入到仿真软件内即可看到效果。

### 文字界面法

1. 打开VSCode，用VSCode打开hal_config.h，将宏定义ECBM_CONFIG_UART_EN的值设为1。

2. 打开hal_uart.h，修改宏定义ECBM_UART_LIB_EN为1。

3. 打开main.c，输入如下代码：

   ```c
   #include "hal_config.h" //hal库配置头文件。
   void main(void){        //主函数。
       uart_typedef uart1;  //定义一个信息结构体，名字叫uart1。
       system_init();      //系统初始化。  
       uart1.id         =1; //设置本结构体为串口1的信息结构体。
       uart1.it_enable  =1; //打开串口1的中断使能。
       uart1.it_priority=0; //设置串口1的中断优先级为低。
       uart1.rxd_enable =1; //打开串口1的接收使能。
       uart1.baud_mul   =0; //设置串口1的波特率不加倍。
       uart1.mux_enable =0; //关闭串口1的多机通信使能。
       uart1.run_mode   =1; //设置串口1的工作模式为可变波特率8位通信。
       uart1.check      =1; //设置串口1的校验方式为无校验。
       uart1.check      =0xFDFD;//设置串口1的定时初值，这个初值对应9600bps@11.0592MHz。
       uart_init(&uart1);   //使用以上信息初始化串口1。
       while(1){           //主循环。
           uart_string(&uart1,"Hi,PC!\r\n");
       }
   }
   ```

4. 编译出HEX文件，下载到单片机或者导入到仿真软件内即可看到效果。

### 效果演示

