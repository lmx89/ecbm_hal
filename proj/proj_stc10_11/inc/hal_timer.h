#ifndef _HAL_TIMER_H_ //头文件防止重加载必备，先看看有没有定义过这个，定义说明已经加载过一次了。
#define _HAL_TIMER_H_ //没定义说明是首次加载，那么往下执行。并且定义这个宏定义，防止下一次被加载。
/*-------------------------------------------------------------------------------------
The MIT License (MIT)

Copyright (c) 2021 奈特

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

免责说明：
    本软件库以MIT开源协议免费向大众提供。作者只保证原始版本是由作者在维护修BUG，
其他通过网络传播的版本也许被二次修改过，由此出现的BUG与作者无关。而当您使用原始
版本出现BUG时，请联系作者解决。
                            **************************
                            * 联系方式：进群778916610 *
                            ************************** 
------------------------------------------------------------------------------------*///这是开源协议，下面是图形界面选项。
//-----------------以下是图形设置界面，可在Configuration Wizard界面设置-----------------
//<<< Use Configuration Wizard in Context Menu >>>

//<e>定时器0函数库使能
//<i>使能该设置，即可让IDE编译和定时器0有关的代码。
//<i>不使能该设置，即可不编译相关代码，从而减小代码占用空间大小。
#define ECBM_TIMER0_LIB_EN 1
//<q>初始化时启动
//<i>使能的话，定时器0会在初始化的时候启动。
//<i>不使能的话，就得手动设置或者用timer_start函数启动。
//0为不在初始化时启动,1为在初始化时启动。
#define ECBM_TIMER0_EN 1
//<o>门控位
//<i>使能的话，除了软件开启外，还需要P3.2脚为高电平才能启动定时器0。
//<i>不使能的话，软件就能控制定时器的开启和关闭。
//<0=>关闭门控 <1=>开启门控，对应P3.2脚
#define ECBM_TIMER0_GATE_EN 0
//<o>运行模式
//<i>定时器本身就是计数器，对内部时钟计数，由于时钟是周期性的且周期时间已知，则可通过t=nT算出时间。
//<i>对外部脉冲计数，就是计数器。来一个脉冲，寄存器就加一。如果外部脉冲也有周期性，那么也可以当定时器用。
//<0=>定时器 <1=>计数器
#define ECBM_TIMER0_RUN_MODE 0
//<o>最大计数位数
//<i>51单片机的定时器最大只有16位。通常8位模式都有自动重装功能，16位模式在比较新的型号里才有自动重装功能。
//<i>最大计数位数决定了定时器能计时或计数的最大值，8位能计到255,16位能计到65535。
//<0=>8位自动重装 <1=>16位
#define ECBM_TIMER0_MAX_BIT 0
//<q>时钟输出
//<i>使能时钟输出之后，每次定时器溢出，会翻转P3.4脚的电平。因此溢出率就是P3.4方波的频率。
#define ECBM_TIMER0_OUT_PUT_EN 1
//<o>定时分频
//<i>当定时器做定时应用的时候，可以选择将系统时钟做一个分频。12分频就是标准8051的速度，1分频就是标准8051的12倍速度。
//<i>总结就是说12分频可以兼容标准8051，1分频可以获得更精细的定时精度（当然定时时间就少了）。
//<0=>12分频 <1=>1分频
#define ECBM_TIMER0_SPEED 0
//<q>中断使能
//<i>打开中断使能后，每次定时器溢出时就会触发中断。对于8位来说是计到256溢出，对于16位来说是计到65536溢出。
#define ECBM_TIMER0_IT_EN 0
//<o>中断优先级
//<i>在同时触发中断的时候，高优先级中断的函数总会比低优先级中断的函数先执行。
//<0=>低优先级 <1=>高优先级
#define ECBM_TIMER0_PRIORITY 0
//<o>初值
//<i>在定时器溢出的时候，若有自动重装功能，就把计数寄存器重装上初值。
//<0-65535>
#define ECBM_TIMER0_VALUE 0
//</e>
//<e>定时器1函数库使能
//<i>使能该设置，即可让IDE编译和定时器1有关的代码。
//<i>不使能该设置，即可不编译相关代码，从而减小代码占用空间大小。
#define ECBM_TIMER1_LIB_EN 0
//<q>初始化时启动
//<i>使能的话，定时器1会在初始化的时候启动。
//<i>不使能的话，就得手动设置或者用timer_start函数启动。
//0为不在初始化时启动,1为在初始化时启动。
#define ECBM_TIMER1_EN 0
//<o>门控位
//<i>使能的话，除了软件开启外，还需要P3.3脚为高电平才能启动定时器1。
//<i>不使能的话，软件就能控制定时器的开启和关闭。
//<0=>关闭门控 <1=>开启门控，对应P3.3脚
#define ECBM_TIMER1_GATE_EN 0
//<o>运行模式
//<i>定时器本身就是计数器，对内部时钟计数，由于时钟是周期性的且周期时间已知，则可通过t=nT算出时间。
//<i>对外部脉冲计数，就是计数器。来一个脉冲，寄存器就加一。如果外部脉冲也有周期性，那么也可以当定时器用。
//<0=>定时器 <1=>计数器
#define ECBM_TIMER1_RUN_MODE 0
//<o>最大计数位数
//<i>51单片机的定时器最大只有16位。通常8位模式都有自动重装功能，16位模式在比较新的型号里才有自动重装功能。
//<i>最大计数位数决定了定时器能计时或计数的最大值，8位能计到255,16位能计到65535。
//<0=>8位自动重装 <1=>16位
#define ECBM_TIMER1_MAX_BIT 0
//<q>时钟输出
//<i>使能时钟输出之后，每次定时器溢出，会翻转P3.5脚的电平。因此溢出率就是P3.5方波的频率。
#define ECBM_TIMER1_OUT_PUT_EN 0
//<o>定时分频
//<i>当定时器做定时应用的时候，可以选择将系统时钟做一个分频。12分频就是标准8051的速度，1分频就是标准8051的12倍速度。
//<i>总结就是说12分频可以兼容标准8051，1分频可以获得更精细的定时精度（当然定时时间就少了）。
//<0=>12分频 <1=>1分频
#define ECBM_TIMER1_SPEED 0
//<q>中断使能
//<i>打开中断使能后，每次定时器溢出时就会触发中断。对于8位来说是计到256溢出，对于16位来说是计到65536溢出。
#define ECBM_TIMER1_IT_EN 0
//<o>中断优先级
//<i>在同时触发中断的时候，高优先级中断的函数总会比低优先级中断的函数先执行。
//<0=>低优先级 <1=>高优先级
#define ECBM_TIMER1_PRIORITY 0
//<o>初值
//<i>在定时器溢出的时候，若有自动重装功能，就把计数寄存器重装上初值。
//<0-65535>
#define ECBM_TIMER1_VALUE 1
//</e>
//<h>函数优化
//<q>timer_reload
//<i>本函数用于重装初值，但是如果用的是自动重装模式，那么这个函数就没有用了。
#define ECBM_TIMER_RELOAD_EN 1
//<q>timer_set_time
//<i>本函数用于计算定时时间的初值，假如初值已经算好，可以优化掉本函数。
#define ECBM_TIMER_SET_TIME_EN 1
//<q>timer_set_count
//<i>本函数用于计算计数数量的初值，假如初值已经算好，可以优化掉本函数。
#define ECBM_TIMER_SET_COUNT_EN 1
//</h>
//<<< end of configuration section >>>
//-----------------以上是图形设置界面，可在Configuration Wizard界面设置-----------------
/*---------------------------------------头文件------------------------------------*/
#include "hal_config.h" //库的头文件。
/*--------------------------------------结构体定义---------------------------------*/
typedef struct{
    u8  id:3;           //定时器编号，0~1。
    u8  enable:1;       //定时器使能，0为关闭，1为开启。
    u8  gate_enable:1;  //定时器门控功能，0为关闭，1为开启。
    u8  output_enable:1;//定时器时钟输出功能，0为关闭，1为开启。
    u8  speed_mode:1;   //定时器定时速度模式，0为12分频的速度，1为1分频的速度。
    u8  run_mode:1;     //定时器工作模式，0为定时器，1为计数器。
    u8  max_bit:1;      //定时器最大位数，0为8位，1为16位。
    u8  it_enable:1;    //定时器中断使能，0为关闭，1为开启。
    u8  it_priority:1;  //定时器优先级，0为低优先级，1为高优先级。
	u16 init_value;     //定时器初值，填写0~65535。
}timer_typedef;
/*---------------------------------------枚举定义----------------------------------*/
typedef enum{
    TIMER_OK=0,         //定时器操作正常。
    TIMER_ID_ERR,       //定时器编号错误。
    TIMER_TIMEOUT_ERR,  //定时器超时错误。
    TIMER_OVERFLOW_ERR, //定时器计数溢出错误。
    TIMER_OCCUPY_WARN,  //定时器被占用警告。
}timer_status;
/*---------------------------------------宏定义------------------------------------*/

/*--------------------------------------程序定义-----------------------------------*/

/*-------------------------------------------------------
函数名：timer_init
描  述：定时器初始化函数。
输  入：dev     定时器信息结构体。
输  出：无
返回值：
    TIMER_OK     正常
    TIMER_ID_ERR 编号错误
创建者：奈特
调用例程：
    timer_typedef tim0;     //定义一个信息结构体。
    tim0.id=0;              //设置本结构体为定时器0的信息结构体。
    tim0.enable=1;          //打开定时器0的使能。
    tim0.gate_enable=0;     //关闭定时器0门控位的使能。
    tim0.output_enable=0;   //关闭定时器0的时钟输出功能。
    tim0.it_enable=0;       //关闭定时器0中断的使能。
    tim0.max_bit=0;         //定时器0的最大位数是8位，通常是8位自动重装。
    tim0.run_mode=0;        //定时器0运行模式是定时器模式。
    tim0.init_value=0xfdfd; //由于是8位自动重装，所以初值是0xfd，重装值也是0xfd。
    timer_init(&tim0);      //使用以上信息初始化定时器0。
创建日期：2021-01-21
修改记录：
-------------------------------------------------------*/
extern timer_status timer_init(timer_typedef * dev);
/*-------------------------------------------------------
函数名：timer_deinit
描  述：定时器还原函数，用于将定时器的设置还原成上电默认值。
输  入：id    定时器编号。
输  出：无
返回值：
    TIMER_OK       正常
    TIMER_ID_ERR   定时器编号错误
创建者：奈特
调用例程：
    timer_deinit(0);//还原定时器0的设置。
创建日期：2021-01-27
修改记录：
-------------------------------------------------------*/
extern timer_status timer_deinit(u8 id);
/*-------------------------------------------------------
函数名：timer_start
描  述：定时器开启函数，用于启动定时器。
输  入：dev     定时器信息结构体。
输  出：无
返回值：
    TIMER_OK      正常
    TIMER_ID_ERR  编号错误
创建者：奈特
调用例程：
    timer_start(&tim0);      //开启定时器0。
创建日期：2021-01-20
修改记录：
-------------------------------------------------------*/
extern timer_status timer_start(timer_typedef * dev);
/*-------------------------------------------------------
函数名：timer_stop
描  述：定时器关闭函数，用于关闭定时器。
输  入：dev     定时器信息结构体。
输  出：无
返回值：
    TIMER_OK     正常
    TIMER_ID_ERR 编号错误
创建者：奈特
调用例程：
    timer_stop(&tim0);      //关闭定时器0。
创建日期：2021-01-20
修改记录：
-------------------------------------------------------*/
extern timer_status timer_stop(timer_typedef * dev);
/*-------------------------------------------------------
函数名：timer_reload
描  述：定时器初值重装函数，只推荐用在16位不能重装模式下，有自动重装的都不推荐这个函数。
输  入：dev     定时器信息结构体。
输  出：无
返回值：无
创建者：奈特
调用例程：
    void tim_fun()TIMER0_IT_NUM{    //定时器0中断。
        timer_reload(&tim0);        //重装初值。
        ...                         //用户自己的代码。
    }
创建日期：2021-01-27
修改记录：
-------------------------------------------------------*/
extern void timer_reload(timer_typedef * dev);
/*-------------------------------------------------------
函数名：timer_set_time
描  述：定时器计算定时初值函数。
输  入：time    定时时间，单位uS。
输  出：dev     定时器信息结构体。
返回值：
    TIMER_OK            正常
    TIMER_TIMEOUT_ERR   定时时间超出当前模式所能运行的最长时间
创建者：奈特
调用例程：
    if(timer_set_time(&tim0,1000)==TIMER_OK){   //定时1000uS,也就是1mS。
        timer_init(&tim0);                      //计算时间成功的话就用这个值初始化定时器0。
    }
创建日期：2021-01-27
修改记录：
-------------------------------------------------------*/
extern timer_status timer_set_time(timer_typedef * dev,u16 time);
/*-------------------------------------------------------
函数名：timer_set_count
描  述：定时器设置计数初值函数。
输  入：count   脉冲计数个数。
输  出：dev     定时器信息结构体。
返回值：
    TIMER_OK            正常
    TIMER_OVERFLOW_ERR  计数的个数超过了当前模式的最大值。
创建者：奈特
调用例程：
    if(timer_set_count(&tim0,100)==TIMER_OK){  //计数100个数。
        timer_init(&tim0);                     //计算成功的话就用这个值初始化定时器0。
    }
创建日期：2021-02-15
修改记录：
-------------------------------------------------------*/
extern timer_status timer_set_count(timer_typedef * dev,u16 count);
/*-------------------------------------------------------
函数名：timer_get_configuration_wizard
描  述：从图形化界面中读取定时器信息函数。
输  入：id      定时器编号。
输  出：dev     定时器信息结构体    
返回值：
    TIMER_OK       正常
    TIMER_ID_ERR   定时器编号错误
创建者：奈特
调用例程：
    timer_typedef tim0; //定义一个信息结构体。
    timer_get_configuration_wizard(&tim0,0);//从图形化界面里读取定时器0的设置信息。
    timer_init(&tim0);  //使用以上信息初始化定时器0。
创建日期：2021-01-27
修改记录：
-------------------------------------------------------*/
extern timer_status timer_get_configuration_wizard(timer_typedef * dev,u8 id);
#endif //和最上面的#ifndef配合成一个程序段。
       //以一个空行为结尾。