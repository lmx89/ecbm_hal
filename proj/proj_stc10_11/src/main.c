#include "hal_config.h" //hal库配置头文件。
timer_typedef tim0;
uart_typedef uart1;
void main(void){        //主函数。
    u8 i,dat;
    system_init();      //系统初始化。
    timer_get_configuration_wizard(&tim0,0);
    timer_init(&tim0);
    uart_get_configuration_wizard(&uart1,1);
    uart_init(&uart1);
    for(i=0;i<10;i++){
        dat=eeprom_read(i);
        uart_char(&uart1,dat);
    }
    while(1){           //主循环。

    }
}

