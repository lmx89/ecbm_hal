#include "hal_config.h"
#if ECBM_CONFIG_EXTI_EN //编译开关，当配置库没有使能该外设时，就不编译该.c文件
/*--------------------------------------程序定义-----------------------------------*/
/*-------------------------------------------------------
外部中断初始化函数。
-------------------------------------------------------*/
exti_status exti_init(exti_typedef * dev){
    switch (dev->id){                        //根据编号来控制对应外部中断寄存器。
        #if ECBM_EXTI0_LIB_EN
            case 0:{                         //以下是控制外部中断0。
                if(dev->it_mode){            //根据结构体信息设置中断模式，
                    EXTI0_SET_MODE_FALLING;  //有下降沿
                }else{                       //和
                    EXTI0_SET_MODE_LOWLEVEL; //低电平
                }                            //两种模式。
                if(dev->it_priority){        //根据结构体信息设置中断优先级，
                    IT_SET_EXTI0_HIGH;       //有高优先级
                }else{                       //和
                    IT_SET_EXTI0_LOW;        //低优先级
                }                            //两种，中断同时发生时，高优先级可以先执行。
                EXTI0_SET_IO_HIGH;           //设置外部中断脚为高电平，51单片机输入必备。
                if(dev->it_enable){          //根据结构体信息设置中断使能，
                    EXTI0_ENABLE;            //打开
                }else{                       //或者
                    EXTI0_DISABLE;           //关闭。
                }
                peripheral.exti|=0x01;       //声明该外设被占用。
            }break;
        #endif
        #if ECBM_EXTI1_LIB_EN
            case 1:{                         //以下是控制外部中断1。
                if(dev->it_mode){            //根据结构体信息设置中断模式，
                    EXTI1_SET_MODE_FALLING;  //有下降沿
                }else{                       //和
                    EXTI1_SET_MODE_LOWLEVEL; //低电平
                }                            //两种模式。
                if(dev->it_priority){        //根据结构体信息设置中断优先级，
                    IT_SET_EXTI1_HIGH;       //有高优先级
                }else{                       //和
                    IT_SET_EXTI1_LOW;        //低优先级
                }                            //两种，中断同时发生时，高优先级可以先执行。
                EXTI1_SET_IO_HIGH;           //设置外部中断脚为高电平，51单片机输入必备。
                if(dev->it_enable){          //根据结构体信息设置中断使能，
                    EXTI1_ENABLE;            //打开
                }else{                       //或者
                    EXTI1_DISABLE;           //关闭。
                }
                peripheral.exti|=0x02;      //声明该外设被占用。
            }break;
        #endif
        default:return  EXTI_ID_ERR;break;   //如果信息里没有以上编号，返回编号错误。
    }
    #if (ECBM_EXTI0_LIB_EN)||(ECBM_EXTI1_LIB_EN)
        return  EXTI_OK;             //以上执行完毕之后，返回OK。
    #endif
}
/*-------------------------------------------------------
外部中断还原函数。
-------------------------------------------------------*/
exti_status exti_deinit(u8 id){
    switch(id){
        #if ECBM_EXTI0_LIB_EN
            case 0:{                    //如果编号是0，
                EXTI0_SET_MODE_LOWLEVEL;//还原成低电平中断。
                IT_SET_EXTI0_LOW;       //还原成低优先级
                EXTI0_SET_IO_HIGH;      //上电都是默认为高电平。
                EXTI0_DISABLE;          //关闭外部中断。
                peripheral.exti&=~0x01; //声明该外设没有被占用。
            }break;
        #endif
        #if ECBM_EXTI1_LIB_EN
            case 1:{                    //如果编号是1，
                EXTI0_SET_MODE_LOWLEVEL;//还原成低电平中断。
                IT_SET_EXTI0_LOW;       //还原成低优先级
                EXTI0_SET_IO_HIGH;      //上电都是默认为高电平。
                EXTI0_DISABLE;          //关闭外部中断。
                peripheral.exti&=~0x02; //声明该外设没有被占用。
            }break;
        #endif
        default:return  EXTI_ID_ERR;break;  //如果id不是以上编号，返回编号错误。
    }
    #if (ECBM_EXTI0_LIB_EN)||(ECBM_EXTI1_LIB_EN)
        return  EXTI_OK;//如果以上语句都执行了，那么就返回OK。
    #endif
}
/*-------------------------------------------------------
外部中断开启函数。
-------------------------------------------------------*/
exti_status exti_start(exti_typedef * dev){
    #if (ECBM_EXTI0_LIB_EN)||(ECBM_EXTI1_LIB_EN)
        dev->it_enable=1;           //既然是开启函数，那么先设置中断使能为开启，
    #endif
    switch (dev->id){           //然后根据编号来设置对应的寄存器。
        #if ECBM_EXTI0_LIB_EN
            case 0:{            //编号是0的时候，
                EXTI0_ENABLE;   //然后打开外部中断0。
            }break;
        #endif
        #if ECBM_EXTI1_LIB_EN
            case 1:{            //编号是1的时候，
                EXTI1_ENABLE;   //然后打开外部中断1。
            }break;
        #endif
        default:return  EXTI_ID_ERR;break;//如果信息里没有以上编号，返回编号错误。
    }
    #if (ECBM_EXTI0_LIB_EN)||(ECBM_EXTI1_LIB_EN)
        return  EXTI_OK;            //能执行到这里，说明上面语句正常执行，返回OK。
    #endif
}
/*-------------------------------------------------------
外部中断关闭函数。
-------------------------------------------------------*/
exti_status exti_stop(exti_typedef * dev){
    #if (ECBM_EXTI0_LIB_EN)||(ECBM_EXTI1_LIB_EN)
        dev->it_enable=0;           //既然是关闭函数，那么先设置中断使能为关闭，
    #endif
    switch (dev->id){           //然后根据编号来设置对应的寄存器。
        #if ECBM_EXTI0_LIB_EN
            case 0:{            //编号是0的时候，
                EXTI0_DISABLE;  //然后关闭外部中断0。
            }break;
        #endif
        #if ECBM_EXTI1_LIB_EN
            case 1:{            //编号是1的时候，
                EXTI1_DISABLE;  //然后关闭外部中断1。
            }break;
        #endif
        default:return  EXTI_ID_ERR;break;//如果信息里没有以上编号，返回编号错误。
    }
    #if (ECBM_EXTI0_LIB_EN)||(ECBM_EXTI1_LIB_EN)
        return  EXTI_OK;            //能执行到这里，说明上面语句正常执行，返回OK。
    #endif
}
/*-------------------------------------------------------
从图形化界面中读取外部中断信息函数。
-------------------------------------------------------*/
exti_status exti_get_configuration_wizard(exti_typedef * dev,u8 id){
    dev=dev;    
    switch(id){                                         //根据输入的编号来读取，
        #if ECBM_EXTI0_LIB_EN
            case 0:{                                    //编号是0的时候，
                dev->id         =0;                     //写入ID为0。不用dev->id=id是为了避免错误ID被储存。
                dev->it_enable  =ECBM_EXTI0_EN;         //写入中断使能设置。
                dev->it_mode    =ECBM_EXTI0_MODE;       //写入中断模式设置。
                dev->it_priority=ECBM_EXTI0_PRIORITY;   //写入中断优先级设置。
            }break;
        #endif
        #if ECBM_EXTI1_LIB_EN
            case 1:{                                    //编号是1的时候，
                dev->id         =1;                     //写入ID为1。
                dev->it_enable  =ECBM_EXTI1_EN;         //写入中断使能设置。
                dev->it_mode    =ECBM_EXTI1_MODE;       //写入中断模式设置。
                dev->it_priority=ECBM_EXTI1_PRIORITY;   //写入中断优先级设置。
            }break;
        #endif
        default:return  EXTI_ID_ERR;break;  //如果id不是以上编号，返回编号错误。
    }
    #if (ECBM_EXTI0_LIB_EN)||(ECBM_EXTI1_LIB_EN)
        return  EXTI_OK;//如果以上语句都执行了，那么就返回OK。
    #endif
}
#endif