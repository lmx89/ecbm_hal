#include "hal_config.h"
#if ECBM_CONFIG_TIMER_EN //编译开关，当配置库没有使能该外设时，就不编译该.c文件
/*--------------------------------------程序定义-----------------------------------*/

/*-------------------------------------------------------
定时器初始化函数。
-------------------------------------------------------*/
timer_status timer_init(timer_typedef * dev){
    switch(dev->id){                        //根据编号来控制对应定时器寄存器。
        #if ECBM_TIMER0_LIB_EN == 1
            case 0:{                        //编号是0的时候，
                if(dev->max_bit){           //根据结构体信息设置最大计数位数。
                    TIMER0_SET_MODE_16BIT;  //有16位的，不过旧型号是16位不自动重装。
                }else{                      //还有
                    TIMER0_SET_MODE_8BIT;   //8位的，一般8位肯定都是自动重装。
                }
                if(dev->run_mode){          //根据结构体信息设置运行模式。
                    TIMER0_SET_MODE_COUNT;  //可以是计外部脉冲的计数器，
                }else{                      //也可以是
                    TIMER0_SET_MODE_TIMER;  //对内部时钟的定时器。
                }
                if(dev->gate_enable){       //根据结构体信息设置门控开关。
                    TIMER0_GATE_ENABLE;     //打开门控，
                }else{                      //或者
                    TIMER0_GATE_DISABLE;    //关上门控。
                }
                if(dev->output_enable){     //根据结构体信息设置时钟是否输出。
                    TIMER0_OUT_ON;          //打开时钟输出，
                }else{                      //或者
                    TIMER0_OUT_OFF;         //关闭时钟输出。
                }
                if(dev->speed_mode){        //根据结构体信息设置定时器速度（分频值）
                    TIMER0_SET_MODE_1T;     //设置1分频，就是标准8051的12倍速。
                }else{                      //或者
                    TIMER0_SET_MODE_12T;    //设置12分频，也就是标准8051的速度。
                }
                if(dev->it_enable){         //根据结构体信息设置中断使能。
                    TIMER0_IT_ENABLE;       //打开使能，
                }else{                      //或者
                    TIMER0_IT_DISABLE;      //关闭使能。
                }
                if(dev->it_priority){       //根据结构体信息设置中断优先级。
                    IT_SET_TIMER0_HIGH;     //有高优先级
                }else{                      //和
                    IT_SET_TIMER0_LOW;      //低优先级。
                }
                TIMER0_SET_REG_HL(dev->init_value);//填写定时器初值。
                if(dev->enable){            //根据结构体信息设置中断使能。
                    TIMER0_POWER_ON;        //打开
                }else{                      //或者
                    TIMER0_POWER_OFF;       //关闭。
                }
                peripheral.timer|=0x01;     //声明该外设被占用。
            }break;
        #endif
        #if ECBM_TIMER1_LIB_EN == 1
            case 1:{                        //编号是1的时候，
                if(dev->max_bit){           //根据结构体信息设置最大计数位数。
                    TIMER1_SET_MODE_16BIT;  //有16位的，不过旧型号是16位不自动重装。
                }else{                      //还有
                    TIMER1_SET_MODE_8BIT;   //8位的，一般8位肯定都是自动重装。
                }
                if(dev->run_mode){          //根据结构体信息设置运行模式。
                    TIMER1_SET_MODE_COUNT;  //可以是计外部脉冲的计数器，
                }else{                      //也可以是
                    TIMER1_SET_MODE_TIMER;  //对内部时钟的定时器。
                }
                if(dev->gate_enable){       //根据结构体信息设置门控开关。
                    TIMER1_GATE_ENABLE;     //打开门控，
                }else{                      //或者
                    TIMER1_GATE_DISABLE;    //关上门控。
                }
                if(dev->output_enable){     //根据结构体信息设置时钟是否输出。
                    TIMER1_OUT_ON;          //打开时钟输出，
                }else{                      //或者
                    TIMER1_OUT_OFF;         //关闭时钟输出。
                }
                if(dev->speed_mode){        //根据结构体信息设置定时器速度（分频值）
                    TIMER1_SET_MODE_1T;     //设置1分频，就是标准8051的12倍速。
                }else{                      //或者
                    TIMER1_SET_MODE_12T;    //设置12分频，也就是标准8051的速度。
                }
                if(dev->it_enable){         //根据结构体信息设置中断使能。
                    TIMER1_IT_ENABLE;       //打开使能，
                }else{                      //或者
                    TIMER1_IT_DISABLE;      //关闭使能。
                }
                if(dev->it_priority){       //根据结构体信息设置中断优先级。
                    IT_SET_TIMER1_HIGH;     //有高优先级
                }else{                      //和
                    IT_SET_TIMER1_LOW;      //低优先级。
                }
                TIMER1_SET_REG_HL(dev->init_value);//填写定时器初值。
                if(dev->enable){            //根据结构体信息设置中断使能。
                    TIMER1_POWER_ON;        //打开
                }else{                      //或者
                    TIMER1_POWER_OFF;       //关闭。
                }
                if((peripheral.uart&0x01)&&(peripheral.timer&0x02)){//如果串口1被定义且定时器1被定义，那么就能判定定时器1被占用。
                    return TIMER_OCCUPY_WARN;
                }
                peripheral.timer|=0x02;     //声明该外设被占用。
            }break;
        #endif
        default:return TIMER_ID_ERR;break;//如果信息里没有以上编号，返回编号错误。
    }
    #if (ECBM_TIMER0_LIB_EN)||(ECBM_TIMER1_LIB_EN)
        return TIMER_OK;                    //以上执行完毕之后，返回OK。
    #endif
}
/*-------------------------------------------------------
定时器还原函数。
-------------------------------------------------------*/
timer_status timer_deinit(u8 id){
    switch(id){
        #if ECBM_TIMER0_LIB_EN == 1
            case 0:{                    //编号是0的时候，
                TIMER0_POWER_OFF;       //关闭定时器0，
                TIMER0_OUT_OFF;         //关闭定时器0的时钟输出，
                TIMER0_IT_DISABLE;      //关闭定时器0中断，
                TIMER0_SET_REG_TMOD(0); //定时器0的模式寄存器清零。
                peripheral.timer&=~0x01;//解除该外设的占用。
            }break;
        #endif
        #if ECBM_TIMER1_LIB_EN == 1
            case 1:{                    //编号是1的时候，
                TIMER1_POWER_OFF;       //关闭定时器1，
                TIMER1_OUT_OFF;         //关闭定时器1的时钟输出，
                TIMER1_IT_DISABLE;      //关闭定时器1中断，
                TIMER1_SET_REG_TMOD(0); //定时器1的模式寄存器清零。
                peripheral.timer&=~0x02;//解除该外设的占用。
            }break;
        #endif
        default:return TIMER_ID_ERR;break;//如果信息里没有以上编号，返回编号错误。
    }
    #if (ECBM_TIMER0_LIB_EN)||(ECBM_TIMER1_LIB_EN)
        return TIMER_OK;//以上语句执行完毕后，返回OK。
    #endif
}
/*-------------------------------------------------------
定时器开启函数。
-------------------------------------------------------*/
timer_status timer_start(timer_typedef * dev){
    #if (ECBM_TIMER0_LIB_EN)||(ECBM_TIMER1_LIB_EN)
        dev->enable=1;                  //既然是开启函数，那么先设置定时器使能为开启，
    #endif
    switch(dev->id){                    //然后根据编号设置打开对应定时器。
        #if ECBM_TIMER0_LIB_EN == 1
            case 0:{                    //编号是0，
                TIMER0_POWER_ON;        //就打开定时器0。
            }break;     
        #endif
        #if ECBM_TIMER1_LIB_EN == 1
            case 1:{                    //编号是1，
                TIMER1_POWER_ON;        //就打开定时器1。
            }break;
        #endif
        default:return TIMER_ID_ERR;break;//如果信息里没有以上编号，返回编号错误。
    }
    #if (ECBM_TIMER0_LIB_EN)||(ECBM_TIMER1_LIB_EN)
        return TIMER_OK;                    //以上执行完毕之后，返回OK。
    #endif
}
/*-------------------------------------------------------
定时器关闭函数。
-------------------------------------------------------*/
timer_status timer_stop(timer_typedef * dev){
    #if (ECBM_TIMER0_LIB_EN)||(ECBM_TIMER1_LIB_EN)
        dev->enable=0;   	              //既然是关闭函数，那么先设置定时器使能为关闭，
    #endif
    switch(dev->id){                      //然后根据编号设置关闭对应定时器。
        #if ECBM_TIMER0_LIB_EN == 1
            case 0:{                      //编号是0，
                TIMER0_POWER_OFF;         //就关闭定时器0。
            }break;
        #endif
        #if ECBM_TIMER1_LIB_EN == 1
            case 1:{                      //编号是1，
                TIMER1_POWER_OFF;         //就关闭定时器1。
            }break;
        #endif
        default:return TIMER_ID_ERR;break;//如果信息里没有以上编号，返回编号错误。
    }
    #if (ECBM_TIMER0_LIB_EN)||(ECBM_TIMER1_LIB_EN)
        return TIMER_OK;                      //以上执行完毕之后，返回OK。
    #endif
}
/*-------------------------------------------------------
定时器初值重装函数。
-------------------------------------------------------*/
#if ECBM_TIMER_RELOAD_EN
void timer_reload(timer_typedef * dev){
    switch(dev->id){                            //通过编号来获知该重装谁，
        #if ECBM_TIMER0_LIB_EN == 1
            case 0:{                                //编号是0的话，
                TIMER0_SET_REG_HL(dev->init_value); //重装定时器0。
            }break;
        #endif
        #if ECBM_TIMER1_LIB_EN == 1
            case 1:{                                //编号是1的话，
                TIMER1_SET_REG_HL(dev->init_value); //重装定时器1。
            }break;
        #endif
    }//没有返回值和错误判断，目的是减少本函数的执行时间，提高整体效率。
}
#endif
/*-------------------------------------------------------
定时器计算初值函数。
-------------------------------------------------------*/
#if ECBM_TIMER_SET_TIME_EN
timer_status timer_set_time(timer_typedef * dev,u16 time){
    u8 temp8;
    u32 temp32;
    switch(dev->max_bit){                   //根据最大位数来做对应的判断。
        case 0:{                            //是8位的时候，
            temp32=(u32)((float)(256.0f*ECBM_SYSCLK_NT)/((float)ECBM_SYSCLK_SETTING/1000000.0f));//根据时钟计算定时的最大值。
            if(time>temp32){                //若是超过了最大值，
                return TIMER_TIMEOUT_ERR;   //返回定时器超时。
            }
            temp8=(u8)((float)(ECBM_SYSCLK_SETTING/ECBM_SYSCLK_NT)*((float)(time)/1000000.0f));//若是没有超过，
            dev->init_value=(256-temp8)*256+(256-temp8);//计算出初值。
        }break;
        case 1:{                            //是16位的时候，
            temp32=(u32)((float)(65536.0f*ECBM_SYSCLK_NT)/((float)ECBM_SYSCLK_SETTING/1000000.0f));//根据时钟计算定时的最大值。
            if(time>temp32){                //若是超过了最大值，
                return TIMER_TIMEOUT_ERR;   //返回定时器超时。
            }
            temp32=(u32)((float)(ECBM_SYSCLK_SETTING/ECBM_SYSCLK_NT)*((float)(time)/1000000.0f));//若是没有超过，
            dev->init_value=(u16)(65536L-temp32);//计算出初值。
        }break;
    }
    return TIMER_OK;                        //以上语句执行没问题就返回OK。
}
#endif
/*-------------------------------------------------------
定时器设置计数初值函数。
-------------------------------------------------------*/
#if ECBM_TIMER_SET_COUNT_EN
timer_status timer_set_count(timer_typedef * dev,u16 count){
    u8 temp8;
    switch(dev->max_bit){                   //通过最大位数来判断，
        case 0:{                            //如果是8位计数器，
            if(count>255){                  //就不能超过255次。
                return TIMER_OVERFLOW_ERR;  //超过就返回溢出。
            }
            temp8=(u8)(count);              //若是没有超过，
            dev->init_value=(256-temp8)*256+(256-temp8);//计算出初值。
        }break;
        case 1:{                            //如果是16位计数器，
            dev->init_value=(u16)(65536-count);//参数就是16位变量，所以不需要判断溢出，直接计算初值。
        }break;
    }
    return TIMER_OK;
}
#endif
/*-------------------------------------------------------
从图形化界面中读取定时器信息函数。
-------------------------------------------------------*/
timer_status timer_get_configuration_wizard(timer_typedef * dev,u8 id){
    dev=dev;
    switch(id){                                     //根据编号来读取对应的设置。
        #if ECBM_TIMER0_LIB_EN == 1
            case 0:{                                    //编号是0的时候，
                dev->id=0;                              //ID设置为0。
                dev->enable     =ECBM_TIMER0_EN;        //读取启动设置。
                dev->gate_enable=ECBM_TIMER0_GATE_EN;   //读取门控位设置。
                dev->output_enable=ECBM_TIMER0_OUT_PUT_EN;//读取时钟输出设置。
                dev->speed_mode =ECBM_TIMER0_SPEED;     //读取速度分频设置。
                dev->max_bit    =ECBM_TIMER0_MAX_BIT;   //读取最大计数值设置。
                dev->run_mode   =ECBM_TIMER0_RUN_MODE;  //读取运行模式设置。
                if(dev->max_bit){
                    dev->init_value=ECBM_TIMER0_VALUE; //读取初值。
                }else{
                    dev->init_value=((u16)((u8)(ECBM_TIMER0_VALUE))<<8)|(u16)((u8)(ECBM_TIMER0_VALUE));
                }
                dev->it_enable  =ECBM_TIMER0_IT_EN;     //读取中断使能设置。
                dev->it_priority=ECBM_TIMER0_PRIORITY;  //读取中断优先级设置。
                
                
            }break;
        #endif
        #if ECBM_TIMER1_LIB_EN == 1
            case 1:{                                    //编号是1的时候，
                dev->id=1;                              //ID设置为1。
                dev->enable     =ECBM_TIMER1_EN;        //读取启动设置。
                dev->gate_enable=ECBM_TIMER1_GATE_EN;   //读取门控位设置。
                dev->output_enable=ECBM_TIMER1_OUT_PUT_EN;//读取时钟输出设置。
                dev->speed_mode =ECBM_TIMER1_SPEED;     //读取速度分频设置。
                dev->max_bit    =ECBM_TIMER1_MAX_BIT;    //读取最大计数值设置。
                dev->run_mode   =ECBM_TIMER1_RUN_MODE;  //读取运行模式设置。
                if(dev->max_bit){
                    dev->init_value=ECBM_TIMER1_VALUE; //读取初值。
                }else{
                    dev->init_value=((u16)((u8)(ECBM_TIMER1_VALUE))<<8)|(u16)((u8)(ECBM_TIMER1_VALUE));
                }
                dev->init_value =ECBM_TIMER1_VALUE;     //读取初值。
                dev->it_enable  =ECBM_TIMER1_IT_EN;      //读取中断使能设置。
                dev->it_priority=ECBM_TIMER1_PRIORITY;  //读取中断优先级设置。
            }break;
        #endif
        default:return TIMER_ID_ERR;break;//如果没有以上编号，返回编号错误。
    }
    #if (ECBM_TIMER0_LIB_EN)||(ECBM_TIMER1_LIB_EN)
        return TIMER_OK;//以上语句执行完毕后，返回OK。
    #endif
}
#endif