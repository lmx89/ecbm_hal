#ifndef _HAL_8051_H_ //头文件防止重加载必备，先看看有没有定义过这个，定义说明已经加载过一次了。
#define _HAL_8051_H_ //没定义说明是首次加载，那么往下执行。并且定义这个宏定义，防止下一次被加载。
/*----------------------------------------------------------------------------------
The MIT License (MIT)

Copyright (c) 2021 奈特

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

免责说明：
    本软件库以MIT开源协议免费向大众提供。作者只保证原始版本是由作者在维护修BUG，
其他通过网络传播的版本也许被二次修改过，由此出现的BUG与作者无关。而当您使用原始
版本出现BUG时，请联系作者解决。
                            **************************
                            * 联系方式：进群778916610 *
                            **************************
---------------------------------------------------------------------------------*/
typedef unsigned    char    u8 ;//无符号字符型，0~255（1个字节）。
typedef signed      char    s8 ;//有符号字符型，-128~127（1个字节）。
typedef unsigned    short   u16;//无符号短整型，0~65535（2个字节）。
typedef signed      short   s16;//有符号短整型，-32768~32767（2个字节）。
typedef unsigned    long    u32;//无符号长整型，0~4294967295（4个字节）。
typedef signed      long    s32;//有符号长整型，-2147483648~2147483647（4个字节）。
#define REG_IN_MASK(reg,mask,value)  do{reg=((reg&(~mask))|((u8)(value)&mask));}while(0)//以掩码的形式向在指定寄存器存入一个值。
/*--------------------------------------CPU--------------------------------------*/
sfr     PSW                         =0xD0;
sbit    CY                          =PSW^7;
sbit    AC                          =PSW^6;
sbit    F0                          =PSW^5;
sbit    RS1                         =PSW^4;
sbit    RS0                         =PSW^3;
sbit    OV                          =PSW^2;
sbit    F1                          =PSW^1;
sbit    P                           =PSW^0;
sfr     ACC                         =0xE0;
sfr     B                           =0xF0;
sfr     SP                          =0x81;
sfr     DPL                         =0x82;
sfr     DPH                         =0x83;
sfr16   DPTR                        =0x82;
#define IF_ODD(_OE_)                ACC=_OE_;if(P)//判断是不是奇数   
/*--------------------------------------GPIO-------------------------------------*/
sfr     P0                          =0x80;
sbit    P00                         =P0^0;
sbit    P01                         =P0^1;
sbit    P02                         =P0^2;
sbit    P03                         =P0^3;
sbit    P04                         =P0^4;
sbit    P05                         =P0^5;
sbit    P06                         =P0^6;
sbit    P07                         =P0^7;
sfr     P1                          =0x90;
sbit    P10                         =P1^0;
sbit    P11                         =P1^1;
sbit    P12                         =P1^2;
sbit    P13                         =P1^3;
sbit    P14                         =P1^4;
sbit    P15                         =P1^5;
sbit    P16                         =P1^6;
sbit    P17                         =P1^7;
sfr     P2                          =0xA0;
sbit    P20                         =P2^0;
sbit    P21                         =P2^1;
sbit    P22                         =P2^2;
sbit    P23                         =P2^3;
sbit    P24                         =P2^4;
sbit    P25                         =P2^5;
sbit    P26                         =P2^6;
sbit    P27                         =P2^7;
sfr     P3                          =0xB0;
sbit    P30                         =P3^0;
sbit    P31                         =P3^1;
sbit    P32                         =P3^2;
sbit    P33                         =P3^3;
sbit    P34                         =P3^4;
sbit    P35                         =P3^5;
sbit    P36                         =P3^6;
sbit    P37                         =P3^7;
/*---------------------------------------IT--------------------------------------*/
sfr     IE                          =0xA8;
sbit    EA                          =IE^7;
sbit    ES                          =IE^4;
sbit    ET1                         =IE^3;
sbit    EX1                         =IE^2;
sbit    ET0                         =IE^1;
sbit    EX0                         =IE^0;
sfr     IP                          =0xB8;
sbit    PS                          =IP^4;
sbit    PT1                         =IP^3;
sbit    PX1                         =IP^2;
sbit    PT0                         =IP^1;
sbit    PX0                         =IP^0;
#define EA_ENABLE                   do{EA=1;}while(0)//打开总中断开关。
#define EA_DISABLE                  do{EA=0;}while(0)//关闭总中断开关。

#define IT_SET_UART1_HIGH           do{PS=1;}while(0)//设置串口的中断优先级为高	
#define IT_SET_UART1_LOW            do{PS=0;}while(0)//设置串口的中断优先级为低

#define IT_SET_TIMER1_HIGH          do{PT1=1;}while(0)//设置定时器1的中断优先级为高	
#define IT_SET_TIMER1_LOW           do{PT1=0;}while(0)//设置定时器1的中断优先级为低

#define IT_SET_EXTI1_HIGH           do{PX1=1;}while(0)//设置外部中断1的中断优先级为高	
#define IT_SET_EXTI1_LOW            do{PX1=0;}while(0)//设置外部中断1的中断优先级为低

#define IT_SET_TIMER0_HIGH          do{PT0=1;}while(0)//设置定时器0的中断优先级为高	
#define IT_SET_TIMER0_LOW           do{PT0=0;}while(0)//设置定时器0的中断优先级为低

#define IT_SET_EXTI0_HIGH           do{PX0=1;}while(0)//设置外部中断0的中断优先级为高	
#define IT_SET_EXTI0_LOW            do{PX0=0;}while(0)//设置外部中断0的中断优先级为低
/*--------------------------------------TIMER------------------------------------*/
/*###############################定时器0################################*/
sfr     TCON                        =0x88;
sbit    TF1                         =TCON^7;
sbit    TR1                         =TCON^6;
sbit    TF0                         =TCON^5;
sbit    TR0                         =TCON^4;
sbit    IE1                         =TCON^3;
sbit    IT1                         =TCON^2;
sbit    IE0                         =TCON^1;
sbit    IT0                         =TCON^0;
sfr     TMOD                        =0x89;
#define T1_GATE                     0x80//定时器1门控位
#define T1_CT                       0x40//定时器1计数控制位
#define T1_M1                       0x20//定时器1模式选择位1
#define T1_M0                       0x10//定时器1模式选择位0
#define T0_GATE                     0x08//定时器0门控位
#define T0_CT                       0x04//定时器0计数控制位
#define T0_M1                       0x02//定时器0模式选择位1
#define T0_M0                       0x01//定时器0模式选择位0
sfr     TL0                         =0x8A;
sfr     TL1                         =0x8B;
sfr     TH0                         =0x8C;
sfr     TH1                         =0x8D;
//-------------功能指令------------//
#define TIMER0_ENABLE               do{TF0=0;TR0=1;}while(0)//打开定时器0
#define TIMER0_DISABLE              do{TR0=0;}while(0)      //关闭定时器0

#define TIMER0_IT_ENABLE            do{ET0=1;}while(0)//打开定时器0中断使能
#define TIMER0_IT_DISABLE           do{ET0=0;}while(0)//关闭定时器0中断使能

#define TIMER0_GATE_ENABLE          do{TMOD|= T0_GATE;}while(0)//打开定时器0的门控位
#define TIMER0_GATE_DISABLE         do{TMOD&=~T0_GATE;}while(0)//关闭定时器0的门控位

#define TIMER0_SET_MODE_COUNT       do{TMOD|= T0_CT;}while(0)//设置定时器0的模式为计数器
#define TIMER0_SET_MODE_TIMER       do{TMOD&=~T0_CT;}while(0)//设置定时器0的模式为定时器

#define TIMER0_SET_MODE_16BIT       REG_IN_MASK(TMOD,0x03,T0_M0)//设置定时器0运行于16位模式
#define TIMER0_SET_MODE_8BIT        REG_IN_MASK(TMOD,0x03,T0_M1)//设置定时器0运行于8位自动重载

#define TIMER0_GET_FLAG             (TF0)               //读取定时器0的溢出标志位
#define TIMER0_FLAG_CLS             do{TF0=0;}while(0)  //清除定时器0的溢出标志位
//-------------寄存器读取----------//
#define TIMER0_SET_REG_TMOD(value)  REG_IN_MASK(TMOD,0x0F,value)//设置定时器0的工作模式
#define TIMER0_GET_REG_TMOD         (TMOD&0x0F)//读取定时器0的工作模式

#define TIMER0_SET_REG_HL(value)    do{TL0=(u8)(value);TH0=(u8)(value>>8);}while(0)//设置定时器0的计数值
#define TIMER0_GET_REG_HL           ((((u16)(TH0))<<8)|(u16)(TL0))//读取定时器0的计数值

#define TIMER0_SET_REG_H8(value)    do{TH0=(u8)(value);}while(0)//设置定时器0的计数值的高8位
#define TIMER0_GET_REG_H8           (TH0)//读取定时器0的计数值的高8位

#define TIMER0_SET_REG_L8(value)    do{TL0=(u8)(value);}while(0)//设置定时器0的计数值的低8位
#define TIMER0_GET_REG_L8           (TL0)//读取定时器0的计数值的低8位
//-------------功能定义------------//
#define TIMER0_IT_NUM               interrupt 1//定时器0的中断号
/*###############################定时器1################################*/
//-------------功能指令------------//
#define TIMER1_ENABLE               do{TF1=0;TR1=1;}while(0)    //打开定时器1
#define TIMER1_DISABLE              do{TR1=0;}while(0)          //关闭定时器1

#define TIMER1_IT_ENABLE            do{ET1=1;}while(0)          //打开定时器1中断使能
#define TIMER1_IT_DISABLE           do{ET1=0;}while(0)          //关闭定时器1中断使能

#define TIMER1_GATE_ENABLE          do{TMOD|= T1_GATE;}while(0) //打开定时器1的门控位
#define TIMER1_GATE_DISABLE         do{TMOD&=~T1_GATE;}while(0) //关闭定时器1的门控位

#define TIMER1_SET_MODE_COUNT       do{TMOD|= T1_CT;}while(0)   //设置定时器1的模式为计数器
#define TIMER1_SET_MODE_TIMER       do{TMOD&=~T1_CT;}while(0)   //设置定时器1的模式为定时器

#define TIMER1_SET_MODE_16BIT       REG_IN_MASK(TMOD,0x30,T1_M0)//设置定时器1运行于16位模式
#define TIMER1_SET_MODE_8BIT        REG_IN_MASK(TMOD,0x30,T1_M1)//设置定时器1运行于8位自动重载

#define TIMER1_GET_FLAG             (TF1)//读取定时器1的溢出标志位
#define TIMER1_FLAG_CLS             do{TF1=0;}while(0)//清除定时器1的溢出标志位
//-------------寄存器读取----------//
#define TIMER1_SET_REG_TMOD(value)  REG_IN_MASK(TMOD,0xF0,value)//设置定时器1的工作模式
#define TIMER1_GET_REG_TMOD         (TMOD&0xF0)//读取定时器1的工作模式

#define TIMER1_SET_REG_HL(value)    do{TL1=(u8)(value);TH1=(u8)(value>>8);}while(0)//设置定时器1的计数值
#define TIMER1_GET_REG_HL           ((((u16)(TH1))<<8)|(u16)(TL1))	//读取定时器1的计数值

#define TIMER1_SET_REG_H8(value)    do{TH1=(u8)(value);}while(0)//设置定时器1的计数值的高8位
#define TIMER1_GET_REG_H8           (TH1)//读取定时器1的计数值的高8位

#define TIMER1_SET_REG_L8(value)    do{TL1=(u8)(value);}while(0)//设置定时器1的计数值的低8位
#define TIMER1_GET_REG_L8           (TL1)//读取定时器1的计数值的低8位
//-------------功能定义------------//
#define TIMER1_IT_NUM               interrupt 3//定时器1的中断号
/*--------------------------------------EXTI-------------------------------------*/
/*##############################外部中断0###############################*/
//-------------功能指令------------//
#define EXTI0_ENABLE                do{EX0=1;}while(0)//打开外部中断0
#define EXTI0_DISABLE               do{EX0=0;}while(0)//关闭外部中断0

#define EXTI0_SET_MODE_FALLING      do{IT0=1;}while(0)//设置外部中断0的工作模式是下降沿中断
#define EXTI0_SET_MODE_LOWLEVEL     do{IT0=0;}while(0)//设置外部中断0的工作模式是低电平中断

#define EXTI0_SET_IO_HIGH           do{P32=1;}while(0)//设置IO的默认电平为高电平
#define EXTI0_GET_IO                (P32)             //通过读IO的电平能知道是上升沿触发还是下降沿触发
//-------------功能定义------------//
#define EXTI0_IT_NUM                interrupt 0//外部中断0的中断号
/*##############################外部中断1###############################*/
//-------------功能指令------------//
#define EXTI1_ENABLE                do{EX1=1;}while(0)//打开外部中断1
#define EXTI1_DISABLE               do{EX1=0;}while(0)//关闭外部中断1

#define EXTI1_SET_MODE_FALLING      do{IT1=1;}while(0)//设置外部中断1的工作模式是下降沿中断
#define EXTI1_SET_MODE_LOWLEVEL     do{IT1=0;}while(0)//设置外部中断1的工作模式是低电平中断

#define EXTI1_SET_IO_HIGH           do{P33=1;}while(0)//设置IO的默认电平为高电平
#define EXTI1_GET_IO                (P33)             //通过读IO的电平能知道是上升沿触发还是下降沿触发
//-------------功能定义------------//
#define EXTI1_IT_NUM                interrupt 2//外部中断1的中断号
/*--------------------------------------UART-------------------------------------*/
sfr     SCON                        =0x98;
sbit    SM0                         =SCON^7; // alternatively "FE"
sbit    FE                          =SCON^7;
sbit    SM1                         =SCON^6;
sbit    SM2                         =SCON^5;
sbit    REN                         =SCON^4;
sbit    TB8                         =SCON^3;
sbit    RB8                         =SCON^2;
sbit    TI                          =SCON^1;
sbit    RI                          =SCON^0;
sfr     SBUF                        =0x99;
sfr     PCON                        =0x87;
#define SMOD                        0x80//串口1波特率控制位
#define SMOD0                       0x40//帧错误检测控制位
#define LVDF                        0x20//低压检测中断请求标志
#define POF                         0x10//上电标志位
#define GF1                         0x08//用户专用标志位
#define GF0                         0x04//用户专用标志位
#define PD                          0x02//掉电模式控制位
#define IDL                         0x01//空闲模式控制位
//-------------功能指令------------//
#define UART1_IT_ENABLE             do{ES=1;}while(0)   //打开串口中断
#define UART1_IT_DISABLE            do{ES=0;}while(0)   //关闭串口中断

#define UART1_TI_CLR                do{TI=0;}while(0)   //清除发送标志位
#define UART1_GET_TI_FLAG           (TI)                //读取发送标志位

#define UART1_RI_CLR                do{RI=0;}while(0)   //清除接受标志位
#define UART1_GET_RI_FLAG           (RI)                //读取接受标志位

#define UART1_SET_TXD_BYTE9_0       do{TB8=0;}while(0)  //设置串口发送的第9位为0
#define UART1_SET_TXD_BYTE9_1       do{TB8=1;}while(0)  //设置串口发送的第9位为1
#define UART1_GET_RXD_BYTE9         (RB8)//读取串口接收的第9位 

#define UART1_SET_BAUD_1            do{PCON&=~SMOD;}while(0)//设置波特率不倍速
#define UART1_SET_BAUD_2            do{PCON|= SMOD;}while(0)//设置波特率二倍速

#define UART1_SET_RXD_ENABLE        do{REN=1;}while(0)//设置串口的接受功能开启
#define UART1_SET_RXD_DISABLE       do{REN=0;}while(0)//设置串口的接受功能关闭

#define UART1_SET_MUX_ENABLE        do{SM2=1;}while(0)//设置串口的多机通信开启
#define UART1_SET_MUX_DISABLE       do{SM2=0;}while(0)//设置串口的多机通信关闭

#define UART1_SET_MODE_S_8          REG_IN_MASK(SCON,0xC0,0x00)//设置串口1的工作模式为同步模式
#define UART1_SET_MODE_A_8_BAUD     REG_IN_MASK(SCON,0xC0,0x40)//设置串口1的工作模式为异步可变波特率8位模式
#define UART1_SET_MODE_A_9          REG_IN_MASK(SCON,0xC0,0x80)//设置串口1的工作模式为异步9位模式
#define UART1_SET_MODE_A_9_BAUD     REG_IN_MASK(SCON,0xC0,0xC0)//设置串口1的工作模式为异步可变波特率9位模式
//-------------寄存器读取----------//
#define UART1_SET_REG_SCON(value)   do{SCON=(u8)(value);}while(0)//设置SCON寄存器
#define UART1_GET_REG_SCON          (SCON)//读取SCON寄存器

#define UART1_SET_REG_SBUF(value)   do{SBUF=(u8)(value);}while(0)//使用串口1发送一个数据
#define UART1_GET_REG_SBUF          (SBUF)//读取串口1接收的数据
//-------------功能定义------------//
#define UART1_IT_NUM                interrupt 4//串口1中断号
#endif