#ifndef _HAL_EXTI_H_ //头文件防止重加载必备，先看看有没有定义过这个，定义说明已经加载过一次了。
#define _HAL_EXTI_H_ //没定义说明是首次加载，那么往下执行。并且定义这个宏定义，防止下一次被加载。
/*-------------------------------------------------------------------------------------
The MIT License (MIT)

Copyright (c) 2021 奈特

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

免责说明：
    本软件库以MIT开源协议免费向大众提供。作者只保证原始版本是由作者在维护修BUG，
其他通过网络传播的版本也许被二次修改过，由此出现的BUG与作者无关。而当您使用原始
版本出现BUG时，请联系作者解决。
                            **************************
                            * 联系方式：进群778916610 *
                            ************************** 
------------------------------------------------------------------------------------*///这是开源协议，下面是图形界面选项。
//-----------------以下是图形设置界面，可在Configuration Wizard界面设置-----------------
//<<< Use Configuration Wizard in Context Menu >>>

//<e>外部中断0函数库使能
//<i>使能该设置，即可让IDE编译和外部中断0有关的代码。
//<i>不使能该设置，即可不编译相关代码，从而减小代码占用空间大小。
#define ECBM_EXTI0_LIB_EN 1
//<q>使能
//<i>使能的话，外部中断脚P3.2在接收到指定状态时会触发中断。
//0为不打开外部中断0,1为打开外部中断0。
#define ECBM_EXTI0_EN 1
//<o>中断模式
//<i>低电平中断，只要是低电平就会一直触发中断。
//<i>下降沿中断，只有高电平跳变到低电平的时候才会中断。
//<0=>低电平中断 <1=>下降沿中断
#define ECBM_EXTI0_MODE 1
//<o>中断优先级
//<i>在同时触发中断的时候，高优先级中断的函数总会比低优先级中断的函数先执行。
//<0=>低优先级 <1=>高优先级
#define ECBM_EXTI0_PRIORITY 0
//</e>
//<e>外部中断1函数库使能
//<i>使能该设置，即可让IDE编译和外部中断1有关的代码。
//<i>不使能该设置，即可不编译相关代码，从而减小代码占用空间大小。
#define ECBM_EXTI1_LIB_EN 1
//<q>使能
//<i>使能的话，外部中断脚P3.3在接收到指定状态时会触发中断。
//0为不打开外部中断1,1为打开外部中断1。
#define ECBM_EXTI1_EN 0
//<o>中断模式
//<i>低电平中断，只要是低电平就会一直触发中断。
//<i>下降沿中断，只有高电平跳变到低电平的时候才会中断。
//<0=>低电平中断 <1=>下降沿中断
#define ECBM_EXTI1_MODE 0
//<o>中断优先级
//<i>在同时触发中断的时候，高优先级中断的函数总会比低优先级中断的函数先执行。
//<0=>低优先级 <1=>高优先级
#define ECBM_EXTI1_PRIORITY 0
//</e>

//<<< end of configuration section >>>
//-----------------以上是图形设置界面，可在Configuration Wizard界面设置-----------------
/*---------------------------------------头文件------------------------------------*/
#include "hal_config.h" //库的头文件。
/*--------------------------------------结构体定义---------------------------------*/
typedef struct {        //外部中断信息结构体。
    u8  id:3;           //外部中断编号，0~1。
    u8  it_enable:1;    //外部中断使能，0为关闭，1为开启。
    u8  it_mode:1;      //外部中断模式，0为低电平中断，1为下降沿中断。
    u8  it_priority:1;  //外部中断优先级，0为低优先级，1为高优先级。
}exti_typedef;
/*---------------------------------------枚举定义----------------------------------*/
typedef enum{           //外部中断系列函数运行信息。
    EXTI_OK=0,          //外部中断操作正常。
    EXTI_ID_ERR         //外部中断编号错误。
}exti_status;
/*--------------------------------------程序定义-----------------------------------*/

/*-------------------------------------------------------
函数名：exti_init
描  述：外部中断初始化函数。
输  入：dev     外部中断信息结构体。
输  出：无
返回值：
    EXTI_OK     正常
    EXTI_ID_ERR 编号错误
创建者：奈特
调用例程：
    exti_typedef exti0; //定义一个信息结构体。
    exti0.id=0;         //设置本结构体为外部中断0的信息结构体。
    exti0.it_enable=1;  //打开外部中断0的中断使能。
    exti0.it_mode=1;    //设置外部中断0的中断模式为下降沿中断。
    exti0.it_priority=1;//设置外部中断0的中断优先级为高。
    exti_init(&exti0);  //使用以上信息初始化外部中断0。
创建日期：2021-01-21
修改记录：
-------------------------------------------------------*/
extern exti_status exti_init(exti_typedef * dev);
/*-------------------------------------------------------
函数名：exti_deinit
描  述：外部中断还原函数，用于还原外部中断的设置到上电默认值。
输  入：id      外部中断编号。
输  出：无
返回值：
    EXTI_OK     正常
    EXTI_ID_ERR 编号错误
创建者：奈特
调用例程：
    exti_deinit(0);  //将外部中断0还原成上电默认的状态。
创建日期：2021-01-26
修改记录：
-------------------------------------------------------*/
extern exti_status exti_deinit(u8 id);
/*-------------------------------------------------------
函数名：exti_start
描  述：外部中断开启函数，用于打开外部中断。
输  入：dev     外部中断信息结构体。
输  出：无
返回值：
    EXTI_OK     正常
    EXTI_ID_ERR 编号错误
创建者：奈特
调用例程：
    exti_start(&exti0); //打开外部中断0。
创建日期：2021-01-21
修改记录：
-------------------------------------------------------*/
extern exti_status exti_start(exti_typedef * dev);
/*-------------------------------------------------------
函数名：exti_stop
描  述：外部中断关闭函数，用于关闭外部中断。
输  入：dev     外部中断信息结构体。
输  出：无
返回值：
    EXTI_OK     正常
    EXTI_ID_ERR 编号错误
创建者：奈特
调用例程：
    exti_stop(&exti0); //关闭外部中断0。
创建日期：2021-01-21
修改记录：
-------------------------------------------------------*/
extern exti_status exti_stop(exti_typedef * dev);
/*-------------------------------------------------------
函数名：exti_get_configuration_wizard
描  述：从图形化界面中读取外部中断信息函数。
输  入：id      需要读取的外部中断编号。
输  出：dev     外部中断信息结构体。
返回值：
    EXTI_OK     正常
    EXTI_ID_ERR 编号错误
创建者：奈特
调用例程：
    exti_typedef exti0; //定义一个信息结构体。
    exti_get_configuration_wizard(&exti0,0);//从图形化界面里读取外部中断0的设置信息。
    exti_init(&exti0);  //使用以上信息初始化外部中断0。
创建日期：2021-01-25
修改记录：
-------------------------------------------------------*/
extern exti_status exti_get_configuration_wizard(exti_typedef * dev,u8 id);
#endif //和最上面的#ifndef配合成一个程序段。
       //以一个空行为结尾。