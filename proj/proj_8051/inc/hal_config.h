#ifndef _HAL_MAIN_H_ //头文件防止重加载必备，先看看有没有定义过这个，定义说明已经加载过一次了。
#define _HAL_MAIN_H_ //没定义说明是首次加载，那么往下执行。并且定义这个宏定义，防止下一次被加载。
/*-------------------------------------------------------------------------------------
The MIT License (MIT)

Copyright (c) 2021 奈特

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

免责说明：
    本软件库以MIT开源协议免费向大众提供。作者只保证原始版本是由作者在维护修BUG，
其他通过网络传播的版本也许被二次修改过，由此出现的BUG与作者无关。而当您使用原始
版本出现BUG时，请联系作者解决。
                            **************************
                            * 联系方式：进群778916610 *
                            ************************** 
------------------------------------------------------------------------------------*///这是开源协议，下面是图形界面选项。
//-----------------以下是图形设置界面，可在Configuration Wizard界面设置-----------------
//<<< Use Configuration Wizard in Context Menu >>>
//<o>系统时钟
//<i>请输入单片机工作的频率，如使用外置晶振则填写晶振频率，如使用内置RC则填写RC振荡频率。
#define ECBM_SYSCLK_SETTING 12000000L
//<o>指令速度
//<i>一般指的是机器周期。比如12T是一个机器周期由12个时钟周期组成。
#define ECBM_SYSCLK_NT      12
//<h>外设库优化
//<i>在单片机内存足够的时候，推荐使能所有外设库。
//<i>在单片机内存不够的时候，可以关闭不用的外设库以减少资源占用。
//<q>EXTI----外部中断库
#define ECBM_CONFIG_EXTI_EN  1
//<q>TIMER---定时器库
#define ECBM_CONFIG_TIMER_EN 1
//<q>UART----串口库
#define ECBM_CONFIG_UART_EN  1
//</h>
//<<< end of configuration section >>>
//-----------------以上是图形设置界面，可在Configuration Wizard界面设置-----------------
/*---------------------------------------头文件------------------------------------*/
#include "hal_8051.h"    //ECBM库的头文件，里面已经包含了STC8的头文件。
#include "hal_exti.h"    //外部中断库的头文件。
#include "hal_timer.h"   //定时器库的头文件。
#include "hal_uart.h"    //串口库的头文件。
/*---------------------------------------宏定义------------------------------------*/
/*-------------------------------------结构体定义----------------------------------*/
typedef struct{     //外设占用信息结构体，用于查看外设的占用情况。
       u8 exti;     //外部中断。
       u8 timer;    //定时器。
       u8 uart;     //串口。
}sys_info_peripheral_typedef;
/*--------------------------------------变量定义-----------------------------------*/
extern sys_info_peripheral_typedef peripheral;//单片机外设使用情况。
/*--------------------------------------程序定义-----------------------------------*/

/*-------------------------------------------------------
函数名：system_init
描  述：系统初始化函数。
输  入：无
输  出：无
返回值：无
创建者：奈特
调用例程：
    void main(void){
        system_init();//初始化整个系统。
        while(1){

        }
    }
创建日期：2021-02-13
修改记录：
-------------------------------------------------------*/
extern void system_init(void);
#endif //和最上面的#ifndef配合成一个程序段。
       //以一个空行为结尾。